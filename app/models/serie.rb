class Serie < ApplicationRecord
  validates :name, presence: true, uniqueness: { scope: :stage_id }
  validates :stage, presence: true

  belongs_to :stage
end
