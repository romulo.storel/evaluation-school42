class Evaluation < ApplicationRecord
  validates :date, :enrollment, :discipline, presence: true

  belongs_to :enrollment
  belongs_to :discipline
end
