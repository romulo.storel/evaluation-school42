class Student < ApplicationRecord
  validates :name, :birth_date, presence: true
  validates :cpf, presence: true, uniqueness: true
end
