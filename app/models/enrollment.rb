class Enrollment < ApplicationRecord
  validates :school, :serie, :student, :start_date, presence: true

  belongs_to :school
  belongs_to :serie
  belongs_to :student
end
