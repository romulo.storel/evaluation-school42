FactoryBot.define do
  factory :student do
    name { Faker::Name.name }
    birth_date { "2010-12-14" }
    cpf { Faker::IDNumber.brazilian_citizen_number }

    trait :age_16_18 do
      birth_date { Faker::Date.birthday(min_age: 16, max_age: 18) }
    end
  end
end
