FactoryBot.define do
  factory :enrollment do
    start_date { "2021-1-1" }
    association :school
    association :serie
    association :student
  end
end
