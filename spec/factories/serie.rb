FactoryBot.define do
  factory :serie do
    name { 'Serie' }
    association :stage
  end
end
