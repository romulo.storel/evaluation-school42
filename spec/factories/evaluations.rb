FactoryBot.define do
  factory :evaluation do
    association :enrollment
    association :discipline
    date { Date.new(2021,4,1) }
  end
end
