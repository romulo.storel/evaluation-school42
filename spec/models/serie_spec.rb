require 'rails_helper'

RSpec.describe Serie, type: :model do
  describe 'validations' do
    it 'validate presence of name' do
      serie = Serie.new(name: nil)

      serie.valid?

      expect(serie.errors[:name]).to include "can't be blank"
    end

    describe 'stage uniqueness validation' do
      context "when it's at the same stage" do
        it 'validate uniqueness of name' do
        end
      end

      context 'when not at the same stage' do
        it 'does not validate uniqueness of name' do
        end
      end
    end
  end
end
