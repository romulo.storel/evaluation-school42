require 'rails_helper'

RSpec.describe Student, type: :model do
  describe 'validations' do
    it 'validate presence of name' do
      student = Student.new(name: nil)

      student.valid?

      expect(student.errors[:name]).to include "can't be blank"
    end

    it 'validate presence of cpf' do
      student = Student.new(cpf: nil)

      student.valid?

      expect(student.errors[:cpf]).to include "can't be blank"
    end

    it 'validate uniqueness of cpf' do
      create(:student, cpf: '00000000000')

      student = Student.new(cpf: '00000000000')
      student.valid?

      expect(student.errors[:cpf]).to include 'has already been taken'
    end

    it 'validate presence of birth_date' do
      student = Student.new(birth_date: nil)

      student.valid?

      expect(student.errors[:birth_date]).to include "can't be blank"
    end
  end
end
