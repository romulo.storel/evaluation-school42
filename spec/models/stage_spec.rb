require 'rails_helper'

RSpec.describe Stage, type: :model do
  describe 'validations' do
    it 'validate presence of name' do
      stage = Stage.new(name: nil)

      stage.valid?

      expect(stage.errors[:name]).to include "can't be blank"
    end

    it 'validate uniqueness of name' do
      create(:stage, name: 'stage')

      stage = Stage.new(name: 'stage')
      stage.valid?

      expect(stage.errors[:name]).to include 'has already been taken'
    end
  end
end
