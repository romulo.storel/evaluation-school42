require 'rails_helper'

RSpec.describe Evaluation, type: :model do
  describe 'validations' do
    it 'validate presence of date' do
      evaluation = Evaluation.new(date: nil)

      evaluation.valid?

      expect(evaluation.errors[:date]).to include "can't be blank"
    end

    it 'validate presence of enrollment' do
      evaluation = Evaluation.new(enrollment: nil)

      evaluation.valid?

      expect(evaluation.errors[:enrollment]).to include "can't be blank"
    end

    it 'validate presence of discipline' do
      evaluation = Evaluation.new(discipline: nil)

      evaluation.valid?

      expect(evaluation.errors[:discipline]).to include "can't be blank"
    end
  end
end
