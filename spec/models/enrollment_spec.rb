require 'rails_helper'

RSpec.describe Enrollment, type: :model do
  describe 'validations' do
    it 'validate presence of start_date' do
      enrollment = Enrollment.new(start_date: nil)

      enrollment.valid?

      expect(enrollment.errors[:start_date]).to include "can't be blank"
    end

    it 'validate presence of school' do
      enrollment = Enrollment.new(school: nil)

      enrollment.valid?

      expect(enrollment.errors[:school]).to include "can't be blank"
    end

    it 'validate presence of serie' do
      enrollment = Enrollment.new(serie: nil)

      enrollment.valid?

      expect(enrollment.errors[:serie]).to include "can't be blank"
    end

    it 'validate presence of student' do
      enrollment = Enrollment.new(student: nil)

      enrollment.valid?

      expect(enrollment.errors[:student]).to include "can't be blank"
    end
  end
end
