require 'rails_helper'

RSpec.describe Discipline, type: :model do
  describe 'validations' do
    it 'validate presence of name' do
      discipline = Discipline.new(name: nil)

      discipline.valid?

      expect(discipline.errors[:name]).to include "can't be blank"
    end

    it 'validate uniqueness of name' do
      create(:discipline, name: 'discipline')

      discipline = Discipline.new(name: 'discipline')
      discipline.valid?

      expect(discipline.errors[:name]).to include 'has already been taken'
    end
  end
end
