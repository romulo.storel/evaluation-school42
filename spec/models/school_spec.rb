require 'rails_helper'

RSpec.describe School, type: :model do
  describe 'validations' do
    it 'validate presence of name' do
      school = School.new(name: nil)

      school.valid?

      expect(school.errors[:name]).to include "can't be blank"
    end

    it 'validate uniqueness of name' do
      create(:school, name: 'school')

      school = School.new(name: 'school')
      school.valid?

      expect(school.errors[:name]).to include 'has already been taken'
    end
  end
end
