# README

## Dependencies
* Ruby 3.0
* Rails 6.1

## Instalation

    run bin/setup

## Running

    rails server

## Tests

    bundle exec rspec spec
