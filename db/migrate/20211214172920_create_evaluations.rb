class CreateEvaluations < ActiveRecord::Migration[6.1]
  def change
    create_table :evaluations do |t|
      t.references :enrollment, null: false, foreign_key: true
      t.references :discipline, null: false, foreign_key: true
      t.decimal :score
      t.date :date

      t.timestamps
    end
  end
end
