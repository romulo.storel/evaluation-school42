require 'faker'

@school_1 = FactoryBot.create(:school, name: 'Escola municipal de Teresina')
@school_2 = FactoryBot.create(:school, name: 'Escola municipal de Natal')

@gramatica = FactoryBot.create(:discipline, name: 'Gramática')
@matematica = FactoryBot.create(:discipline, name: 'Matemática')
@ciencias = FactoryBot.create(:discipline, name: 'Ciências')
@artes = FactoryBot.create(:discipline, name: 'Artes')
@historia = FactoryBot.create(:discipline, name: 'História')

stage_1 = FactoryBot.create(:stage, name: 'Educação Infantil')
stage_2 = FactoryBot.create(:stage, name: 'Ensino Fundamental')
stage_3 = FactoryBot.create(:stage, name: 'Ensino Médio')

@serie_1_2 = FactoryBot.create(:serie, name: '2º Ano', stage: stage_1)
@serie_3_2 = FactoryBot.create(:serie, name: '2º Ano', stage: stage_3)
@serie_2_6 = FactoryBot.create(:serie, name: '6º Ano', stage: stage_2)

@serie_2_2 = FactoryBot.create(:serie, name: '2º Ano', stage: stage_2)
@serie_2_3 = FactoryBot.create(:serie, name: '3º Ano', stage: stage_2)
@serie_2_1 = FactoryBot.create(:serie, name: '1º Ano', stage: stage_2)
@serie_2_5 = FactoryBot.create(:serie, name: '5º Ano', stage: stage_2)
@serie_1_1 = FactoryBot.create(:serie, name: '1º Ano', stage: stage_1)
@serie_3_1 = FactoryBot.create(:serie, name: '1º Ano', stage: stage_3)
@serie_3_3 = FactoryBot.create(:serie, name: '3º Ano', stage: stage_3)
@serie_2_4 = FactoryBot.create(:serie, name: '4º Ano', stage: stage_2)
@serie_2_7 = FactoryBot.create(:serie, name: '7º Ano', stage: stage_2)
@serie_2_8 = FactoryBot.create(:serie, name: '8º Ano', stage: stage_2)
@serie_2_9 = FactoryBot.create(:serie, name: '9º Ano', stage: stage_2)


def self.enroll(student, school)
  series = [
    [@serie_1_1, 3],
    [@serie_1_2, 4],
    [@serie_2_1, 5],
    [@serie_2_2, 6],
    [@serie_2_3, 7],
    [@serie_2_4, 8],
    [@serie_2_5, 9],
    [@serie_2_6, 10],
    [@serie_2_7, 11],
    [@serie_2_8, 12],
    [@serie_2_9, 13],
    [@serie_3_1, 14],
    [@serie_3_2, 15],
    [@serie_3_3, 16]
  ]

  series.each do |serie|
    enroll_start = (student.birth_date + serie[1].year).beginning_of_year + 1.month
    enrollment = FactoryBot.create(:enrollment, id: rand(1...9999999), school: school, serie: serie[0], student: student, start_date: student.birth_date + serie[1])

    # Gramatica
    FactoryBot.create(:evaluation, enrollment: enrollment, discipline: @gramatica, score: Random.rand(5..10), date: enroll_start + 2.month)
    FactoryBot.create(:evaluation, enrollment: enrollment, discipline: @gramatica, score: Random.rand(5..10), date: enroll_start + 4.month)
    FactoryBot.create(:evaluation, enrollment: enrollment, discipline: @gramatica, score: Random.rand(5..10), date: enroll_start + 6.month)
    FactoryBot.create(:evaluation, enrollment: enrollment, discipline: @gramatica, score: Random.rand(5..10), date: enroll_start + 8.month)

    # Matemática
    FactoryBot.create(:evaluation, enrollment: enrollment, discipline: @matematica, score: Random.rand(5..10), date: enroll_start + 2.month)
    FactoryBot.create(:evaluation, enrollment: enrollment, discipline: @matematica, score: Random.rand(5..10), date: enroll_start + 4.month)
    FactoryBot.create(:evaluation, enrollment: enrollment, discipline: @matematica, score: Random.rand(5..10), date: enroll_start + 6.month)
    FactoryBot.create(:evaluation, enrollment: enrollment, discipline: @matematica, score: Random.rand(5..10), date: enroll_start + 8.month)

    # Ciências
    FactoryBot.create(:evaluation, enrollment: enrollment, discipline: @ciencias, score: Random.rand(5..10), date: enroll_start + 2.month)
    FactoryBot.create(:evaluation, enrollment: enrollment, discipline: @ciencias, score: Random.rand(5..10), date: enroll_start + 4.month)
    FactoryBot.create(:evaluation, enrollment: enrollment, discipline: @ciencias, score: Random.rand(5..10), date: enroll_start + 6.month)
    FactoryBot.create(:evaluation, enrollment: enrollment, discipline: @ciencias, score: Random.rand(5..10), date: enroll_start + 8.month)

    # Artes
    FactoryBot.create(:evaluation, enrollment: enrollment, discipline: @artes, score: Random.rand(5..10), date: enroll_start + 2.month)
    FactoryBot.create(:evaluation, enrollment: enrollment, discipline: @artes, score: Random.rand(5..10), date: enroll_start + 4.month)
    FactoryBot.create(:evaluation, enrollment: enrollment, discipline: @artes, score: Random.rand(5..10), date: enroll_start + 6.month)
    FactoryBot.create(:evaluation, enrollment: enrollment, discipline: @artes, score: Random.rand(5..10), date: enroll_start + 8.month)

    # História
    FactoryBot.create(:evaluation, enrollment: enrollment, discipline: @historia, score: Random.rand(5..10), date: enroll_start + 2.month)
    FactoryBot.create(:evaluation, enrollment: enrollment, discipline: @historia, score: Random.rand(5..10), date: enroll_start + 4.month)
    FactoryBot.create(:evaluation, enrollment: enrollment, discipline: @historia, score: Random.rand(5..10), date: enroll_start + 6.month)
    FactoryBot.create(:evaluation, enrollment: enrollment, discipline: @historia, score: Random.rand(5..10), date: enroll_start + 8.month)
  end
end

student_1 = FactoryBot.create(:student, :age_16_18)
student_2 = FactoryBot.create(:student, :age_16_18)
student_3 = FactoryBot.create(:student, :age_16_18)
student_4 = FactoryBot.create(:student, :age_16_18)
student_5 = FactoryBot.create(:student, :age_16_18)
student_6 = FactoryBot.create(:student, :age_16_18)
student_7 = FactoryBot.create(:student, :age_16_18)
student_8 = FactoryBot.create(:student, :age_16_18)
student_9 = FactoryBot.create(:student, :age_16_18)
student_10 = FactoryBot.create(:student, :age_16_18)

enroll(student_1, @school_1)
enroll(student_2, @school_1)
enroll(student_3, @school_1)
enroll(student_4, @school_1)
enroll(student_5, @school_2)
enroll(student_6, @school_2)
enroll(student_7, @school_2)
enroll(student_8, @school_2)
enroll(student_9, @school_2)
enroll(student_10, @school_2)
